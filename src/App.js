import React, { Component } from 'react';
import Header from './components/Navigation/Header';
import Home from './components/Home';
import About from './components/About';
import Projects from './components/Projects';
import Contact from './components/Contact';
import Navigation from './components/Navigation/Navigation';
import { BrowserRouter as Router,Route,Switch } from 'react-router-dom';

class App extends Component {
  render(){
    return (
      <Router>
        <div className="App">
           <Header/>
           <Navigation/>
           <div className="container pt-5 pb-5">
             <div className="row">
               <Switch>
                 <Route exact path="/" component={Home}/>
                 <Route exact path="/portfolio" component={Home}/>
                 <Route path="/about" component={About}/>
                 <Route path="/home" component={Home}/>
                 <Route path="/projects" component={Projects}/>
                 <Route path="/contact" component={Contact}/>
               </Switch>
             </div>
           </div>
        </div>
      </Router>
     );
  }
}

export default App;
