import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
class Header extends Component {
    componentDidMount(){
        window.addEventListener("scroll",this.handleScroll);
    }
    componentWillUnmount(){
        window.removeEventListener("scroll",this.handleScroll);
    }
    handleScroll() {
        var position = window.pageYOffset || document.documentElement.scrollTop;
        var navbar = document.querySelector(".navbar").classList;
        if(position > 0){
            navbar.remove("bg-blur1");
            navbar.add("bg-blur2");
        }
        else{
            navbar.remove("bg-blur2");
            navbar.add("bg-blur1");
        }
    }
    render(){
        return (
            <nav className="navbar fixed-top navbar-expand-lg navbar-dark bg-blur1 dn-small">
                <NavLink className="navbar-brand" to="/home">Home</NavLink>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/home">Home</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/about">About me</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/projects">Projects</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/contact">Contact me</NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Header;