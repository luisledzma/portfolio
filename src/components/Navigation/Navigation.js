import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser as faUserS,faHdd as faHddS,faAddressBook as faAddressBookS } from '@fortawesome/free-solid-svg-icons';
import { faUser as faUserR,faHdd as faHddR,faAddressBook as faAddressBookR } from '@fortawesome/free-regular-svg-icons'
class Navigation extends Component {

    constructor(props){
        super(props);
        this.state = {
            about: {
                icon:faUserR,
                color:"#C8C8C8"
            },
            projects:{
                icon:faHddR,
                color:"#C8C8C8"
            },
            contact:{
                icon:faAddressBookR,
                color:"#C8C8C8"
            }
         }
    }

    handleActiveElement = (e) =>{
        if(e === "about"){
            this.setState({
                about: {
                    icon:faUserS,
                    color:"#FFFFFF"
                }
            });
        }
        else{
            this.setState({
                about: {
                    icon:faUserR,
                    color:"#C8C8C8"
                }
            });
        }

        if(e === "projects"){
            this.setState({
                projects: {
                    icon:faHddS,
                    color:"#FFFFFF"
                }
            });
        }
        else{
            this.setState({
                projects: {
                    icon:faHddR,
                    color:"#C8C8C8"
                }
            });
        }

        if(e === "contact"){
            this.setState({
                contact: {
                    icon:faAddressBookS,
                    color:"#FFFFFF"
                }
            });
        }
        else{
            this.setState({
                contact: {
                    icon:faAddressBookR,
                    color:"#C8C8C8"
                }
            });
        }

    }
    
    render(){
        return (
            <nav className="navbar fixed-bottom navbar-expand-lg navbar-dark bg-blur2 db-small dn-medium">
                <div>
                    <ul className="navbar-nav list-group-horizontal">
                        <li className="nav-item ml-5 mr-5">
                            <NavLink className="nav-link" onClick={() => this.handleActiveElement("about")} to="/about"><FontAwesomeIcon color={this.state.about.color} icon={this.state.about.icon} size="2x"/></NavLink>
                        </li>
                        <li className="nav-item ml-5 mr-5">
                            <NavLink className="nav-link" onClick={() => this.handleActiveElement("projects")} to="/projects"><FontAwesomeIcon color={this.state.projects.color} icon={this.state.projects.icon} size="2x"/></NavLink>
                        </li>
                        <li className="nav-item ml-5 mr-5">
                            <NavLink className="nav-link" onClick={() => this.handleActiveElement("contact")} to="/contact"><FontAwesomeIcon color={this.state.contact.color} icon={this.state.contact.icon} size="2x"/></NavLink>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Navigation;